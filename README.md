*****
Zawiera:

AMSR-7805-NZ - regulator napięcia do 5V /

https://www.tme.eu/pl/Document/5bb82d6795c16168832d15f44065fa14/AMSR-78-NZ.pdf

ILSI HC49US-FF5F18-16.0000 - kwarc 16 MHz

https://www.tme.eu/pl/Document/6c671fc2d1c29550257ca2d207d0534f/HC49US_Series.pdf

DC_Jack_55-21 Wtyk zasilania 5.5/2.1 mm

https://www.sparkfun.com/datasheets/Prototyping/Barrel-Connector-PJ-202A.pdf


Microswitch Schurter 1301.9301 (6x6 mm)

PTR MESSTECHNIK AK100/2DS-5,0-V/GRAU - złącze śrubowe 1 rząd x 2 porty

https://www.tme.eu/pl/details/tb-5.0-p-2p-12/listwy-zaciskowe-do-druku/ptr-messtechnik/ak1002ds-50-vgrau/



*****
Jak używać??:

Pobierz plik spod adresu:
https://gitlab.com/afya/afya-kicad-libraries/-/archive/master/afya-kicad-libraries-master.zip

i rozpakuj w dowolnyn folderze (np.: /home/~/afya.pretty), następnie:

1. w PcbNew:
ustawienia -> Footprinty -> dodaj bibliotekę
wpisz:

https://gitlab.com/afya/afya-kicad-libraries/-/archive/master/afya-kicad-libraries-master.zip jako repozytoriun GitHub

2. dodaj do Eeschema
-> ustawienia -> schematy -> przeglądaj/dodaj bibliotekę:
 /home/~/afya.pretty/afya1.lib
